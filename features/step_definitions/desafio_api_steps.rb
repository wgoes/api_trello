Quando("faço a requisição GET para o serviço do trello.") do
  uri_base = "https://api.trello.com/1/actions/592f11060f95a3d3d46a987a"
  $response = HTTParty.get(uri_base)
end

Então("Deve ser exibido o campo Name da estrutura list.") do
  data = $response.parsed_response["data"]["list"]["name"]
  puts data
end

Então("deve ser validado o status code da resposta do serviço.") do
  puts $response.code
  expect($response.code).to eql 200
end
