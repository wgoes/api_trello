#language: pt

Funcionalidade: API Trello

Cenário: Validar GET API Trello
Quando faço a requisição GET para o serviço do trello.
Então Deve ser exibido o campo Name da estrutura list.
E deve ser validado o status code da resposta do serviço.